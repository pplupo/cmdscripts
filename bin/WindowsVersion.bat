@echo off
setlocal
for /f "tokens=2 delims=[]" %%i in ('ver') do set VERSION=%%i
for /f "tokens=2-3 delims=. " %%i in ("%VERSION%") do set VERSION=%%i.%%j
if "%VERSION%" == "1.01" echo Windows 1.0
if "%VERSION%" == "1.02" echo Windows 1.02
if "%VERSION%" == "1.03" echo Windows 1.03
if "%VERSION%" == "1.04" echo Windows 1.04
if "%VERSION%" == "2.03" echo Windows 2.03
if "%VERSION%" == "2.10" echo Windows 2.10
if "%VERSION%" == "2.11" echo Windows 2.11
if "%VERSION%" == "3.00" echo Windows 3.0
if "%VERSION%" == "3.10" echo Windows 3.1
if "%VERSION%" == "3.1" echo Windows NT 3.1
if "%VERSION%" == "3.11" echo Windows 3.11
if "%VERSION%" == "3.2" echo Windows 3.2
if "%VERSION%" == "3.5" echo Windows NT 3.5
if "%VERSION%" == "3.50" echo Windows NT 3.5
if "%VERSION%" == "3.51" echo Windows NT 3.51
if "%VERSION%" == "4.00" echo Windows 95
if "%VERSION%" == "4.03" echo Windows 95
if "%VERSION%" == "4.0" echo Windows NT 4.0
if "%VERSION%" == "4.10" echo Windows 98
if "%VERSION%" == "4.90" echo Windows ME
REM only the versions below were tested
if "%VERSION%" == "5.00" echo Windows 2000
if "%VERSION%" == "5.0" echo Windows 2000
if "%VERSION%" == "5.1" echo Windows XP
if "%VERSION%" == "5.2" echo Windows Server 2003
if "%VERSION%" == "6.0" echo Windows Vista
if "%VERSION%" == "6.1" echo Windows 7
if "%VERSION%" == "6.2" echo Windows 8
if "%VERSION%" == "6.3" echo Windows 8.1
if "%VERSION%" == "6.4" echo Windows 10
if "%VERSION%" == "10.0" echo Windows 10
echo %VERSION%
endlocal